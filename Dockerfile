FROM openjdk:11
ADD target/CustomersFromDatabase-0.0.1-SNAPSHOT.jar a6.jar
ENTRYPOINT [ "java", "-jar", "a6.jar" ]