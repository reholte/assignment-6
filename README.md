# Assignment 6
Spring Boot application in Java about data access with JDBC and Thymeleaf. Using the Chinook database that models the iTunes database of customers purchasing songs.

### Maintainers
- Tone Eggan Pokorny: @tonepok
- Ragnhild Emblem Holte: @reholte

### Program description
The program creates endpoints for the application structure, the Thymeleaf pages and also the REST endpoints. We then handle the Chinook database, for example read a customer, update a customer or show a customers favourite genre. The data is then shown using Thymeleaf. And deployed to Heroku using Docker. 

### Install
- Clone the repo with SSH
- Open the folder in Intellij

### How to run
Run by right-clicking CustomersFromDatabaseApplication and select Run 'CustomersFromDatabaseApplication.main()'

### Heroku
https://salty-stream-61749.herokuapp.com/

### Notes
Created a page for all customers with links to each customers own page. This was not a part of the assignment, but we wanted to do it. 
