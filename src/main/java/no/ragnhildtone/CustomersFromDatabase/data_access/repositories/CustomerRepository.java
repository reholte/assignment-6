package no.ragnhildtone.CustomersFromDatabase.data_access.repositories;

import no.ragnhildtone.CustomersFromDatabase.models.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


import java.util.ArrayList;

public interface CustomerRepository {
    ArrayList<Customer> getAllCustomers();
    Customer getCustomerById(@PathVariable String id);
    Customer getCustomerByName(@PathVariable String name);
    ArrayList<Customer> customerPage(@RequestParam String limit, String offset);
    Boolean newCustomer(@RequestParam int id, String firstName, String lastName, String zipcode, String phone, String mail);
    Boolean updateCustomer(@RequestParam int id, String colName, String value);
    Boolean deleteCustomer(@PathVariable String id);
    ArrayList<CustomerCountry> customersNumberPerCountry();
    ArrayList<CustomerSpender>  highestSpendingCustomers();
    ArrayList<CustomerGenre> customersGenre(@PathVariable String id);
    ArrayList<Song> getRandomSongs();
    ArrayList<Artist> getRandomArtists();
    ArrayList<Genre> getRandomGenres();
    Result search(@PathVariable String song);
}
