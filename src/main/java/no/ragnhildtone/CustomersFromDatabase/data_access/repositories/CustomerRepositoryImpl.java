package no.ragnhildtone.CustomersFromDatabase.data_access.repositories;

import no.ragnhildtone.CustomersFromDatabase.data_access.ConnectionHelper;
import no.ragnhildtone.CustomersFromDatabase.logging.LogToConsole;
import no.ragnhildtone.CustomersFromDatabase.models.*;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{
    private LogToConsole logger;

    private final String url = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public CustomerRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return customers;
    }

    public Customer getCustomerById(String id){
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT * FROM 'Customer' WHERE CustomerId = ?");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return customer;
    }

    // Task 3
    public Customer getCustomerByName(String name){
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT * FROM 'Customer' WHERE FirstName LIKE ?");
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return customer;
    }

    //Task 4
    public ArrayList<Customer> customerPage(String limit, String offset){
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer LIMIT ? OFFSET ?");
            preparedStatement.setString(1, limit);
            preparedStatement.setString(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return customers;
    }

    //Task 5
    public Boolean newCustomer(int id, String firstName, String lastName, String zipcode, String phone, String mail){
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "INSERT INTO Customer (CustomerId, FirstName, LastName, PostalCode, Phone, Email) VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, firstName);
            preparedStatement.setString(3, lastName);
            preparedStatement.setString(4, zipcode);
            preparedStatement.setString(5, phone);
            preparedStatement.setString(6, mail);
            preparedStatement.executeUpdate();
        } catch (Exception e){
            System.out.println("Error: "+e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return true;
    }


    //Task 6
    public Boolean updateCustomer(int id, String colName, String value){
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "UPDATE Customer SET "+colName+" = ? WHERE CustomerId = ?");
            preparedStatement.setString(1, value);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        } catch (Exception e){
            System.out.println("1. Error: "+e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return true;
    }


    // Task 7
    public ArrayList<CustomerCountry> customersNumberPerCountry(){
        ArrayList<CustomerCountry> customerCountries = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT COUNT(CustomerID), Country FROM Customer GROUP BY Country ORDER BY COUNT(CustomerID) DESC");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customerCountries.add(
                        new CustomerCountry(
                                resultSet.getString("Country"),
                                resultSet.getInt("Count(CustomerID)")
                        )
                );
            }
        } catch (Exception e){
            System.out.println("Error: "+e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return customerCountries;
    }

    // Task 8
    public ArrayList<CustomerSpender> highestSpendingCustomers(){
        Customer customer;
        ArrayList<CustomerSpender> customerSpenders = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT c.CustomerId, c.FirstName, c.LastName, c.Country, c.PostalCode, c.Phone, c.Email, i.total FROM Customer as c LEFT JOIN Invoice as i ON i.CustomerId = c.CustomerId ORDER BY Total DESC LIMIT 20"); //How much LIMIT should we have?
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
                customerSpenders.add(
                        new CustomerSpender(
                                customer,
                                resultSet.getFloat("Total")
                        )
                );
            }
        } catch (Exception e){
            System.out.println("Error: "+e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return customerSpenders;
    }

    //Task 9
    public ArrayList<CustomerGenre> customersGenre(String id){
        Customer customer;
        ArrayList<CustomerGenre> customerGenre = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "WITH summary AS (SELECT Genre.Name AS Name, RANK() OVER (ORDER BY COUNT(Genre.Name) DESC) AS rank FROM Genre INNER JOIN Track ON Genre.GenreID = Track.GenreId INNER JOIN InvoiceLine ON Track.TrackId = InvoiceLine.TrackId INNER JOIN Invoice ON InvoiceLine.InvoiceId = Invoice.InvoiceId INNER JOIN Customer ON Invoice.CustomerID = Customer.CustomerId WHERE Customer.CustomerId = ? GROUP BY Genre.Name ORDER BY COUNT(Genre.Name) DESC) SELECT Name FROM summary WHERE rank = 1");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customerGenre.add(
                        new CustomerGenre(
                                resultSet.getString("Name")
                        )
                );
            }
        } catch (Exception e){
            System.out.println("Error: "+e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return customerGenre;
    }

    //delete customer
    public Boolean deleteCustomer(String id){
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "DELETE FROM Customer WHERE CustomerId = ?");
            preparedStatement.setString(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e){
            System.out.println("Error: "+e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return true;
    }

    public Result search(String song){
        song = "%" + song + "%";
        Result result = null;
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Track.Name AS Song, Track.Composer AS Composer, Album.Title AS Album, Genre.Name AS Genre FROM Track INNER JOIN Album ON Track.AlbumId = Album.AlbumId INNER JOIN Genre ON Track.GenreId = Genre.GenreId WHERE UPPER(Track.Name) LIKE UPPER(?)");
            preparedStatement.setString(1, song);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                result = new Result(
                        resultSet.getString("Song"),
                        resultSet.getString("Composer"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre")
                );

            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return result;
    }

    public ArrayList<Artist> getRandomArtists(){
        ArrayList<Artist> artists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM 'Artist' ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                artists.add(
                        new Artist(
                                resultSet.getInt("ArtistId"),
                                resultSet.getString("Name")
                        )
                );
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return artists;
    }

    public ArrayList<Genre> getRandomGenres(){
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM 'Genre' ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                genres.add(
                        new Genre(
                                resultSet.getInt("GenreId"),
                                resultSet.getString("Name")
                        )
                );
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return genres;
    }

    public ArrayList<Song> getRandomSongs(){
        ArrayList<Song> songs = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection is up and running");
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT TrackId, Name, Composer FROM 'Track' ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                songs.add(
                        new Song(
                                resultSet.getInt("TrackId"),
                                resultSet.getString("Name"),
                                resultSet.getString("Composer")
                                )
                );
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            try{
                conn.close();
            } catch (Exception e){
                System.out.println("Can't close: "+e.getMessage());
            }
        }
        return songs;
    }
}
