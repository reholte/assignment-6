package no.ragnhildtone.CustomersFromDatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CustomersFromDatabaseApplication {
	public static void main(String[] args) {SpringApplication.run(CustomersFromDatabaseApplication.class, args);
	}

}
