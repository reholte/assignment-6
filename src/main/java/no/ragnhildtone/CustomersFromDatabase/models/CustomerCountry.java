package no.ragnhildtone.CustomersFromDatabase.models;

public class CustomerCountry {
    private String country;
    private int numberOfCustomers;

    public CustomerCountry(String country, int number) {
        this.country = country;
        numberOfCustomers = number;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

}
