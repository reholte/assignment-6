package no.ragnhildtone.CustomersFromDatabase.models;

public class CustomerSpender {
    private float spender;

    private Customer customer;

    public CustomerSpender(Customer c, float spender) {
        this.spender = spender;
        this.customer = c;
    }

    public float getSpender() {
        return spender;
    }

    public void setSpender(int spender) {
        this.spender = spender;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}