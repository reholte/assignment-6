package no.ragnhildtone.CustomersFromDatabase.models;

public class Song {
    private int trackId;
    private String name, composer;

    public Song(int trackId, String name, String composer) {
        this.trackId = trackId;
        this.name = name;
        this.composer = composer;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }
}
