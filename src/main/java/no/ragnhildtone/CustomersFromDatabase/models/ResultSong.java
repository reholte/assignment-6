package no.ragnhildtone.CustomersFromDatabase.models;

public class ResultSong {
    private String title;
    public ResultSong() {
    }

    public ResultSong(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
