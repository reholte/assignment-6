package no.ragnhildtone.CustomersFromDatabase.controllers;

import no.ragnhildtone.CustomersFromDatabase.data_access.repositories.CustomerRepository;
import no.ragnhildtone.CustomersFromDatabase.data_access.repositories.CustomerRepositoryImpl;
import no.ragnhildtone.CustomersFromDatabase.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
public class CostumerController {
    private final CustomerRepository customerRepository;

    public CostumerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value="/api/customers/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable String id){
        return customerRepository.getCustomerById(id);
    }

    @RequestMapping(value= "/api/customers/FirstName={name}", method = RequestMethod.GET)
    public Customer getCustomerByName(@PathVariable String name){
        return customerRepository.getCustomerByName(name);
    }

    @RequestMapping(value= "/api/customers/page", method = RequestMethod.GET)
    public ArrayList<Customer> customerPage(@RequestParam String limit, String offset){ return customerRepository.customerPage(limit, offset); }

    @RequestMapping(value = "/api/customers/new", method = RequestMethod.POST)
    public Boolean newCustomer(@RequestParam int id, String firstName, String lastName, String zipcode, String phone, String mail){
        return customerRepository.newCustomer(id, firstName, lastName, zipcode, phone, mail);
    }

    @RequestMapping(value = "/api/customers/update", method = RequestMethod.PUT)
    public Boolean updateCustomer(@RequestParam int id, String colName, String value){
        return customerRepository.updateCustomer(id, colName, value);
    }

    @RequestMapping(value="/api/customers/delete", method = {RequestMethod.DELETE, RequestMethod.GET})
    public Boolean deleteCustomer(@RequestParam String id){
        return customerRepository.deleteCustomer(id);
    }

    @RequestMapping("/api/customers/countries")
    public ArrayList<CustomerCountry> customersNumberPerCountry(){
        return customerRepository.customersNumberPerCountry();
    }
    @RequestMapping ("/api/customers/highest")
    public ArrayList<CustomerSpender>  highestSpendingCustomers(){
        return customerRepository.highestSpendingCustomers();
    }
    @RequestMapping("/api/customers/{id}/genre")
    public ArrayList<CustomerGenre> customersGenre(@PathVariable String id){
        return customerRepository.customersGenre(id);
    }

    @RequestMapping("/api/song")
    public Result search(@PathVariable String song){
        return customerRepository.search(song);
    }

}
