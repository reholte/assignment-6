package no.ragnhildtone.CustomersFromDatabase.controllers;

import no.ragnhildtone.CustomersFromDatabase.data_access.repositories.CustomerRepository;
import no.ragnhildtone.CustomersFromDatabase.models.Result;
import no.ragnhildtone.CustomersFromDatabase.models.ResultSong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CustomerThymeleafController {
    final
    CustomerRepository customerRepository;

    public CustomerThymeleafController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping("/")
    public String register() {
        return "redirect:/home";
    }


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("result", new ResultSong());
        model.addAttribute("artists", customerRepository.getRandomArtists());
        model.addAttribute("songs", customerRepository.getRandomSongs());
        model.addAttribute("genres", customerRepository.getRandomGenres());
        return "home";
    }

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String customers(Model model){
        model.addAttribute("customers", customerRepository.getAllCustomers());
        return "customers";
    }
    @RequestMapping(value = "/customers/{id}")
    public String getSpecificCustomer(@PathVariable String id, Model model){
        model.addAttribute("customer", customerRepository.getCustomerById(id));
        return "customer";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String result(@ModelAttribute ResultSong title, BindingResult error, Model model){
        Result result = customerRepository.search(title.getTitle());
        model.addAttribute("result", result);
        return "result";
    }

}
